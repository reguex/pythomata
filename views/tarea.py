# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!
import sys
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
nodes = []
transitions = {}
class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.originCombo = QtWidgets.QComboBox(self.centralwidget)
        self.originCombo.setObjectName("originCombo")
        self.horizontalLayout_2.addWidget(self.originCombo)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.destinationCombo = QtWidgets.QComboBox(self.centralwidget)
        self.destinationCombo.setObjectName("destinationCombo")
        self.horizontalLayout_2.addWidget(self.destinationCombo)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        self.transitionNameTextBox = QtWidgets.QLineEdit(self.centralwidget)
        self.transitionNameTextBox.setObjectName("transitionNameTextBox")
        self.horizontalLayout_2.addWidget(self.transitionNameTextBox)
        self.addTransitionButton = QtWidgets.QPushButton(self.centralwidget)
        self.addTransitionButton.setObjectName("addTransitionButton")
        self.horizontalLayout_2.addWidget(self.addTransitionButton)
        self.gridLayout.addLayout(self.horizontalLayout_2, 4, 0, 1, 1)
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setSizeConstraint(QLayout.SetMaximumSize)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.drawArea = QWidget(self.centralwidget)
        self.drawArea.setObjectName("drawArea")
        self.drawArea.setStyleSheet('QWidget#drawArea {color: white}')
        self.drawArea.paintEvent = self.paintDrawArea
        self.verticalLayout_2.addWidget(self.drawArea)
        self.gridLayout.addLayout(self.verticalLayout_2, 6, 0, 1, 1)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QLayout.SetMinimumSize)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.chainLabel = QLineEdit(self.centralwidget)
        self.chainLabel.setObjectName("chainLabel")
        self.horizontalLayout.addWidget(self.chainLabel)
        self.evaluateButton = QPushButton(self.centralwidget)
        self.evaluateButton.setObjectName("evaluateButton")
        self.horizontalLayout.addWidget(self.evaluateButton)
        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setGeometry(QRect(0, 0, 800, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        self.drawArea.mouseDoubleClickEvent = self.testDoubleClick
        self.addTransitionButton.mousePressEvent = self.addTransition
        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_3.setText(_translate("MainWindow", "Origen"))
        self.label_2.setText(_translate("MainWindow", "Destino"))
        self.label_4.setText(_translate("MainWindow", "Transicion"))
        self.addTransitionButton.setText(_translate("MainWindow", "Agregar Transicion"))
        self.label.setText(_translate("MainWindow", "Cadena: "))
        self.evaluateButton.setText(_translate("MainWindow", "Evaluar"))

    def testDoubleClick(self, someArg: QMouseEvent):
        nodes.append(Node(self.drawArea, someArg.pos(), "Q"+str(len(nodes))))
        self.originCombo.addItem(nodes[-1].name)
        self.destinationCombo.addItem(nodes[-1].name)
    
    def addTransition(self, event):
        origin = None
        destination = None
        for node in nodes:
            if node.name == self.originCombo.currentText():
                origin = node
        for node in nodes:
            if node.name == self.destinationCombo.currentText():
                destination = node
        if origin and destination:
            values = transitions.get(origin.name, {})
            if self.transitionNameTextBox.text() in values:
                msgBox = QMessageBox()
                msgBox.setWindowTitle("Error!")
                msgBox.setText('Ya existe una transicion con este valor!')
                msgBox.addButton(QPushButton('Ok'), QMessageBox.YesRole)
                ret = msgBox.exec_()
                return
            values[self.transitionNameTextBox.text()] =  destination.name
            transitions[origin.name] = values
            self.drawArea.update()

    def paintDrawArea(self, paintEvent):
        p = QPainter(self.drawArea)
        p.fillRect(QRect(0, 0, self.drawArea.width(), self.drawArea.height()), QBrush(QColor(255, 255, 255)))
        p.setPen(QPen(QBrush(QColor(0, 0, 0)), 1))
        p.drawLine(0, 0, self.drawArea.width(), 0)
        p.drawLine(0, 0, 0, self.drawArea.height())
        p.drawLine(self.drawArea.width(), 0, self.drawArea.width()-1, self.drawArea.height()-1)
        p.drawLine(0, self.drawArea.height(), self.drawArea.width()-1, self.drawArea.height()-1)
        self.drawTransitions(p)
    
    def drawTransitions(self, p:QPainter):
        p.setPen(QPen(QBrush(QColor(0, 0, 0)), 1))
        for origin, transition in transitions.items():
            for transitionName, destination in transition.items():
                originNode = next(node for node in nodes if node.name == origin)
                destinationNode = next(node for node in nodes if node.name == destination)
                p.drawLine(originNode.pos, destinationNode.pos)
                p.drawEllipse(destinationNode.pos-QPoint(50, 50), 5, 5)
                p.drawText(QRect(originNode.pos, destinationNode.pos), Qt.AlignCenter, transitionName)

class Node(QtWidgets.QLabel):
    def __init__(self, parentWidget, pos, name, isAcceptanceState = False, isInitialState = False):
        super().__init__(parentWidget)
        self.setParent(parentWidget)
        self.pos = self.mapFromParent(pos)
        self.name = name
        self.isInitialState = isInitialState
        self.isAcceptanceState = isAcceptanceState
        self.setFixedSize(100, 100)
        self.move(self.mapToParent(self.pos - QPoint(50, 50)))
        self.show()
        self.raise_()
        self.paintEvent = self.drawNode
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.showContextMenu)

        self.update()

    def drawNode(self, paintEvent):
        p = QPainter(self)
        p.setRenderHint(QPainter.Antialiasing, True)
        if self.isInitialState:
            p.setBrush(QBrush(QColor(64, 128, 64)))
        else:
            p.setBrush(QBrush(QColor(128, 128, 128)))
        p.drawEllipse(25, 25, 50, 50)
        if self.isAcceptanceState:
            p.drawEllipse(30, 30, 40, 40)
        p.drawText(self.rect(), Qt.AlignCenter, self.name)
    
    def showContextMenu(self, pos):
        contextMenu = QMenu("Context Menu", self)
        action1 = QAction("Seleccionar como Estado Inicial" if not self.isInitialState else "Quitar como Estado Inicial", self)
        action1.triggered.connect(self.setInitialState)
        contextMenu.addAction(action1)
        action2 = QAction("Sleccionar como Estado de Aceptacion" if not self.isAcceptanceState else "Quitar como Estado de Aceptacion", self)
        action2.triggered.connect(self.setAcceptanceState)
        contextMenu.addAction(action2)
        contextMenu.exec(self.mapToGlobal(pos))

    def setInitialState(self, event):
        for node in nodes:
            if node.isInitialState:
                msgBox = QMessageBox()
                msgBox.setWindowTitle("Error!")
                msgBox.setText('No puede haber mas de un estado de aceptacion!')
                msgBox.addButton(QPushButton('Ok'), QMessageBox.YesRole)
                ret = msgBox.exec_()
                return
        self.isInitialState = not self.isInitialState
        self.update()

    def setAcceptanceState(self, event):
        self.isAcceptanceState = not self.isAcceptanceState
        self.update()

if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
    