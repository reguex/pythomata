def is_empty(seq):
    return len(seq) == 0


def powerset(iterable):
    from itertools import chain, combinations
    s = list(iterable)
    return set(chain.from_iterable(combinations(s, r) for r in range(len(s) + 1)))


def join_ordered(iterable: iter, separator: str = ',') -> str:
    return separator.join(str(i) for i in sorted(iterable))
