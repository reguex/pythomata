class Dfa:
    def __init__(self, start: str, finals: set, transitions: dict):
        self.start = start
        self.finals = finals
        self.transitions = transitions

    def evaluate(self, word: str) -> bool:
        current = self.start
        for c in word:
            if current not in self.transitions or c not in self.transitions[current]:
                return False
            current = self.transitions[current][c]
        return current in self.finals

    @property
    def states(self):
        result = set()
        for origin, transitions in self.transitions.items():
            result.add(origin)
            for _, destinies in transitions.items():
                result.add(destinies)
        return result
