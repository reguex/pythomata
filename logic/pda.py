from enum import Enum

from logic.utils import is_empty
from itertools import product
from functools import reduce


class Transition:
    def __init__(self, *, origin: str, value: str, pop: str, push: iter, destiny: str):
        # if len(pop) != 1:
        #     raise BaseException('pop must be of len 1')
        #
        # if len(value) != 1:
        #     raise BaseException('value must be of len 1')

        self.origin = origin
        self.value = value
        self.pop = pop
        self.push = push
        self.destiny = destiny

    def __hash__(self) -> int:
        return hash(self.origin) ^ hash(self.value) ^ hash(self.pop) ^ hash(self.destiny) ^ \
               reduce(lambda y, x: hash(y) ^ hash(x), self.push, 0)

    def __eq__(self, other) -> bool:
        return self.origin == other.origin and self.value == other.value and \
               self.pop == other.pop and self.destiny == other.destiny and self.push == other.push

    def __str__(self) -> str:
        return f"({self.origin}, {self.value}, {self.pop}) -> ({self.destiny}, {self.push})"


class Production:
    def __init__(self, current_state: str, stack: list, word: str):
        self.state = current_state
        self.stack = stack
        self.word = word

    def __hash__(self) -> int:
        return hash(self.state) ^ hash(self.word) ^ reduce(lambda x, y: hash(y) ^ hash(x), self.stack, 0)

    def __eq__(self, other) -> bool:
        return self.word == other.word and self.stack == other.word and self.state == other.current_state

    def __str__(self) -> str:
        return f"({self.state}, {self.word}, {self.stack})"


def grammar_from_file(file_src: str) -> list:
    productions = []
    with open(file_src) as file:
        for line in file.readlines():
            variable, production = line.replace('\n', '').split(' -> ', maxsplit=2)
            productions.append(ProductionRule(variable, production.split(' ')))
    return productions


class Criteria(Enum):
    acceptance_state = 'acceptance_state'
    empty_stack = 'empty_stack'


class ProductionRule:
    def __init__(self, variable: str, production: list):
        self.variable = variable
        self.production = production

    def __str__(self) -> str:
        return f"{self.variable} -> {' '.join(self.production)}"

    def __eq__(self, o) -> bool:
        return isinstance(o, ProductionRule) and self.variable == o.variable and self.production == o.production

    def __hash__(self) -> int:
        return hash(self.variable) ^ hash(self.production)


EPSILON = '$'


class Automaton:
    def __init__(self):
        self.states = set()
        self.alphabet = set()
        self.accepted_states = set()
        self.initial_state = None
        self.initial_stack_input = None
        self.transitions = set()
        self.acceptance_criteria = Criteria.empty_stack

    @staticmethod
    def from_inputs(initial, finals, transitions):
        new_pda = Automaton()
        new_pda.set_initial_state(initial)
        for state in finals:
            new_pda.add_accepted_state(state)
        for origin, state_transitions in transitions.items():
            for (pop, value), moves in state_transitions.items():
                for (destination, push) in moves:
                    new_pda.add_transition(origin=origin, value=value, pop=pop, push=push, destiny=destination)
        return new_pda

    @staticmethod
    def from_grammar_rules(rules: list):
        rule_variables = set(rule.variable for rule in rules)
        q = 'q'

        new_pda = Automaton()
        new_pda.set_acceptance_criteria(Criteria.empty_stack)
        new_pda.set_initial_state(q)
        new_pda.add_accepted_state(q)

        if not is_empty(rules):
            initial_stack_input = rules[0].variable
            new_pda.set_initial_stack_input(initial_stack_input)

        for rule in rules:
            new_pda.add_transition(origin=q, destiny=q, value=EPSILON, pop=rule.variable, push=rule.production)

            for production_input in rule.production:
                if production_input not in rule_variables:
                    new_pda.add_transition(origin=q, destiny=q, value=production_input, pop=production_input, push=[])

        return new_pda

    def add_state(self, state: str):
        self.states.add(state)

    def add_symbol(self, symbol: str):
        self.alphabet.add(symbol)

    def add_accepted_state(self, state: str):
        self.add_state(state)
        self.accepted_states.add(state)

    def set_initial_state(self, state: str):
        self.add_state(state)
        self.initial_state = state

    def set_initial_stack_input(self, symbol: str):
        self.initial_stack_input = symbol

    def set_acceptance_criteria(self, acceptance_criteria: Criteria):
        self.acceptance_criteria = acceptance_criteria

    def add_transition(self, *, origin: str, value: str, pop: str, push: iter, destiny: str):
        self.add_state(origin)
        self.add_state(destiny)
        if value is not EPSILON:
            self.add_symbol(value)

        self.transitions.add(Transition(origin=origin, value=value, pop=pop, push=push, destiny=destiny))

    def find_transitions(self, *, origin=None, value=None, pop=None) -> set:
        return set(transition for transition in self.transitions
                   if (origin is None or transition.origin == origin) and
                   (value is None or transition.value == value) and
                   (pop is None or transition.pop == pop))

    def language_contains(self, word: str, verbose: bool = False) -> bool:
        if self.initial_state is None:
            raise BaseException('undefined initial state')
        if self.initial_stack_input is None:
            raise BaseException('undefined initial stack input')

        initial_production = Production(self.initial_state, [self.initial_stack_input], word)
        # found_productions = {initial_production}
        unprocessed_productions = [initial_production]

        current_index = 0
        while current_index < len(unprocessed_productions):
            current_production = unprocessed_productions[current_index]
            current_index += 1

            if verbose:
                print(f"Current Productions: {current_production}")
                print()

            if self._matches_acceptance_criteria(current_production):
                return True

            next_productions = self._branch_production(current_production)
            for production in next_productions:
                # if production in found_productions:
                #     continue
                if self.acceptance_criteria is Criteria.empty_stack and len(production.stack) > len(word):
                    continue

                unprocessed_productions.append(production)
                # found_productions.add(production)

        return False

    def _branch_production(self, production: Production) -> set:
        branched_productions = set()

        if not is_empty(production.word):
            read_input = next(iter(production.word))
            read_input_productions = self._branch_productions_on_input(production, read_input)
            branched_productions = branched_productions.union(read_input_productions)

        epsilon_productions = self._branch_productions_on_input(production, EPSILON)
        branched_productions = branched_productions.union(epsilon_productions)

        return branched_productions

    def _branch_productions_on_input(self, production: Production, input_symbol: str) -> set:
        if is_empty(production.stack):
            return set()

        popped = next(iter(production.stack))
        next_word = production.word
        if input_symbol != EPSILON:
            next_word = next_word[1:]

        branched_productions = set()

        value_moves = self.find_transitions(origin=production.state, value=input_symbol, pop=popped)
        for move in value_moves:
            next_stack = production.stack[1:]
            next_stack = list(move.push) + next_stack

            new_production = Production(move.destiny, next_stack, next_word)
            branched_productions.add(new_production)

        return branched_productions

    def _matches_acceptance_criteria(self, production: Production) -> bool:
        if not is_empty(production.word):
            return False

        return (self.acceptance_criteria is Criteria.empty_stack and is_empty(production.stack)) or \
               (self.acceptance_criteria is Criteria.acceptance_state and production.state in self.accepted_states)

    @property
    def grammar(self) -> list:
        if not self.initial_state:
            raise BaseException("Undefined initial state.")
        if not self.initial_stack_input:
            raise BaseException("Undefined initial stack input.")

        productions = []

        if self.acceptance_criteria is Criteria.acceptance_state:
            initial_production_states = self.accepted_states
        elif self.acceptance_criteria is Criteria.empty_stack:
            initial_production_states = self.states
        else:
            raise BaseException(f"Unrecognized acceptance method {self.acceptance_criteria}")

        for state in initial_production_states:
            new_production = [f"{self.initial_state}|{self.initial_stack_input}|{state}"]
            productions.append(ProductionRule(self.initial_stack_input, new_production))

        for transition in self.transitions:
            if is_empty(transition.push):
                q, x, a, p = transition.origin, transition.pop, transition.value, transition.destiny
                productions.append(ProductionRule(f"{q}|{x}|{p}", [a]))

        for transition in self.transitions:
            y = transition.push
            k = len(y)
            if k == 0:
                continue
            super_table = self.super_table(k)
            for r in super_table:
                q, x, a, p = transition.origin, transition.pop, transition.value, transition.destiny
                new_variable = f"{q}|{x}|{r[k-1]}"
                new_production = []
                if a != EPSILON:
                    new_production.append(a)
                new_production.append(f"{p}|{y[0]}|{r[0]}")
                for i in range(0, k - 1):
                    new_production.append(f"{r[i]}|{y[i+1]}|{r[i+1]}")
                productions.append(ProductionRule(new_variable, new_production))

        return productions

    def super_table(self, count: int) -> list:
        return list(product(self.states, repeat=count))


if __name__ == '__main__':
    # pda = Automaton()
    # pda.add_transition(origin='q', value='0', pop='X', push='XX', destiny='q')
    # pda.add_transition(origin='q', value='0', pop='Z', push='XZ', destiny='q')
    # pda.add_transition(origin='q', value='1', pop='X', push='', destiny='r')
    # pda.add_transition(origin='r', value='1', pop='X', push='', destiny='r')
    # pda.add_transition(origin='r', value=EPSILON, pop='Z', push='', destiny='r')
    #
    # pda.add_accepted_state('r')
    # pda.set_initial_state('q')
    #
    # pda.set_initial_stack_input('Z')
    # pda.set_acceptance_criteria(Criteria.empty_stack)
    # for g in pda.grammar:
    #     print(g)

    # other_pda = Automaton.from_grammar_rules(pda.grammar)
    # for tr in other_pda.transitions:
    #     print(tr)
    #
    # print(other_pda.language_contains('00000000001111111111'))
    #
    pda_rules = [
        ProductionRule(variable="E", production=["E", "+", "T"]),
        ProductionRule(variable="E", production=["E", "-", "T"]),
        ProductionRule(variable="E", production=["T"]),
        ProductionRule(variable="T", production=["T", "*", "F"]),
        ProductionRule(variable="T", production=["T", "/", "F"]),
        ProductionRule(variable="T", production=["F"]),
        ProductionRule(variable="F", production=["digit"]),
        ProductionRule(variable="digit", production=["0"]),
        ProductionRule(variable="digit", production=["1"])
    ]

    pda = Automaton.from_grammar_rules(pda_rules)
    # for gr in pda.grammar:
    #     print(gr)
    print(pda.language_contains('1+1-1-1--+1-1/1-0', verbose=False))

    # g = grammar_from_file('/Users/eherrera/Desktop/m')
    # for p in g:
    #     print(p.production)
    # pda = Automaton.from_grammar_rules(g)
    # for t in pda.transitions:
    #     print(t)
