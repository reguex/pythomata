from logic.utils import powerset, join_ordered


class Nfa:
    def __init__(self, start: str, finals: set, transitions: dict):
        self.start = start
        self.finals = finals
        self.transitions = transitions

    def evaluate(self, word: str) -> bool:
        current = self._closure({self.start})
        for c in word:
            current = self._closure(self._exits(current, c))
        return any(state in self.finals for state in current)

    def as_dfa(self):
        new_automaton = Nfa(None, set(), dict())
        start = self._closure({self.start})
        new_automaton.start = join_ordered(start)
        if any(state in self.finals for state in start):
            new_automaton.finals.add(join_ordered(start))

        unprocessed_states = [start]
        while unprocessed_states:
            current_state = unprocessed_states.pop()
            new_automaton.transitions[join_ordered(current_state)] = {}
            for symbol in self.alphabet:
                destiny = self._closure(self._exits(self._closure(current_state), symbol))
                if join_ordered(destiny) not in new_automaton.states:
                    unprocessed_states.append(destiny)
                new_automaton.transitions[join_ordered(current_state)][symbol] = {join_ordered(destiny)}
                if any(state in self.finals for state in destiny):
                    new_automaton.finals.add(join_ordered(destiny))
        print(new_automaton.states)
        return new_automaton

    def clear_unreachable_states(self):
        for state in self.unreachable_states:
            del self.transitions[state]

    def clearing_epsilon(self):
        start = self._closure({self.start})
        states = [start]
        queue = list(states)
        transitions = {}
        finals = {join_ordered(start)} if any(state in self.finals for state in start) else set()
        while queue:
            current_set = queue.pop()
            if len(current_set) == 0:
                continue

            transitions[join_ordered(current_set)] = {}
            for symbol in self.alphabet.difference({'$'}):
                new_set = self._closure(self._exits(current_set, symbol))
                if len(new_set) == 0:
                    continue
                transitions[join_ordered(current_set)][symbol] = {join_ordered(new_set)}
                if any(state in self.finals for state in new_set):
                    finals.add(join_ordered(new_set))
                if new_set not in states:
                    states.append(new_set)
                    queue.append(new_set)
        return Nfa(join_ordered(start), finals, transitions)

    @property
    def unreachable_states(self) -> set:
        def process(r, q):
            temp = set()
            for state in q:
                for symbol in self.alphabet:
                    temp = temp.union(self._exits({state}, symbol))
                    q = temp.difference(r)
                    r = r.union(q)
            return r, q

        reachable_states, queue = process({self.start}, {self.start})
        while queue:
            reachable_states, queue = process(reachable_states, queue)
        return self.states.difference(reachable_states)

    @property
    def alphabet(self) -> set:
        return set(symbol for transitions in self.transitions.values() for symbol in transitions)

    @property
    def states(self) -> set:
        result = set()
        for origin, transitions in self.transitions.items():
            result.add(origin)
            for destinies in transitions.values():
                result = result.union(destinies)
        return result

    def _closure(self, states: set) -> set:
        queue = list(states)
        result = set(states)
        while queue:
            current = queue.pop()
            exits = self._exits({current}, '$')
            for state in exits:
                if state not in result:
                    queue.append(state)
                    result.add(state)
        return result

    def _exits(self, states: set, c: str) -> set:
        exits = set()
        for state in states:
            if state in self.transitions and c in self.transitions[state]:
                exits = exits.union(self.transitions[state][c])
        return exits

    def transition(self, q, c):
        return self.transitions[q][c] if self.transitions.get(q) and self.transitions[q].get(c) else set()

    @property
    def complement(self):
        start = self.start
        transitions = {}
        finals = set()

        for state in self.states:
            if state not in self.finals:
                finals.add(state)

        acceptance_state = "ACCEPTANCE_STATE"
        finals.add(acceptance_state)

        transitions[acceptance_state] = {}
        for c in self.alphabet.difference({'$'}):
            transitions[acceptance_state][c] = {acceptance_state}

        for state in self.states:
            transitions[state] = {}
            for c in self.alphabet:
                transitions[state][c] = self.transitions.get(state, {}).get(c) or {acceptance_state}

        return Nfa(start, finals, transitions)

    def union(self, other_nfa):
        def is_valid(state_product):
            return any(state in self.finals for state in state_product.automaton_a_set) or \
                   any(state in other_nfa.finals for state in state_product.automaton_b_set)

        product = self._product(other_nfa, is_valid)
        return product

    def intersect(self, other):
        def is_valid(state_product):
            return any(state in self.finals for state in state_product.automaton_a_set) and \
                   any(state in other.finals for state in state_product.automaton_b_set)

        product = self._product(other, is_valid)
        return product

    def _product(self, other_nfa, is_acceptance):
        start = ProductState(self._closure({self.start}), other_nfa._closure({other_nfa.start}))

        unprocessed_states = [start]
        states = {start}
        transitions = {}
        acceptance_states = set()

        if is_acceptance(start):
            acceptance_states.add(start.value)

        while unprocessed_states:
            current_state = unprocessed_states.pop()
            transitions[current_state.value] = {}
            for symbol in self.alphabet.union(other_nfa.alphabet):
                transitions[current_state.value][symbol] = set()
                self_destinies = self._closure(self._exits(current_state.automaton_a_set, symbol))
                other_destinies = other_nfa._closure(other_nfa._exits(current_state.automaton_b_set, symbol))
                state_product = ProductState(self_destinies, other_destinies)

                if state_product not in states:
                    states.add(state_product)
                    unprocessed_states.append(state_product)

                if is_acceptance(current_state):
                    acceptance_states.add(current_state.value)

                transitions[current_state.value][symbol].add(state_product.value)

        return Nfa(start.value, acceptance_states, transitions)

    def _transitions_on_to(self, on, contained_in) -> set:
        found_transitions = set()
        for origin, transitions in self.transitions.items():
            for t, destinies in transitions.items():
                if t == on and any(state in contained_in for state in destinies):
                    found_transitions.add(origin)
        return found_transitions

    @property
    def _state_partitions(self):
        P = [self.finals, self.states.difference(self.finals)]
        W = [self.finals]

        while W:
            A = W.pop()
            for c in self.alphabet:
                X = self._transitions_on_to(c, A)
                for Y in P:
                    if X.intersection(Y) and Y.difference(X):
                        P.remove(Y)
                        if X.intersection(Y) not in P:
                            P.append(X.intersection(Y))
                        if Y.difference(X) not in P:
                            P.append(Y.difference(X))

                        if Y in W:
                            W.remove(Y)
                            if X.intersection(Y) not in W:
                                W.append(X.intersection(Y))
                            if Y.difference(X) not in W:
                                W.append(Y.difference(X))
                        else:
                            if len(X.intersection(Y)) <= len(Y.difference(X)):
                                if X.intersection(Y) not in W:
                                    W.append(X.intersection(Y))
                            else:
                                if Y.difference(X) not in W:
                                    W.append(Y.difference(X))
        return P

    def minimize(self):
        return self.clear_unreachable_states().remove_equivalent_states()

    def remove_equivalent_states(self):
        unequivalent = Nfa(None, [], {})
        for p in self._state_partitions:
            for input in self.alphabet:
                destiny = join_ordered(self._exits(p, input))
                p_destiny = next(part for part in p if all(state in destiny for state in part))
                e = unequivalent.transitions.get(join_ordered(p), {})
                e[input] = join_ordered(p_destiny)
                unequivalent[join_ordered(p)] = e
                if any(s in self.finals for s in p_destiny):
                    unequivalent.finals.add(join_ordered(p_destiny))

        return unequivalent

    def regex(self):
        import re
        initial = object()
        final = object()
        states = {initial, final} | set(self.states)
        expr = {}
        for x in states:
            for y in states:
                expr[x, y] = None
        for x in self.states:
            if x == self.start:
                expr[initial, x] = ''
            if x in self.finals:
                expr[x, final] = ''
            expr[x, x] = ''
        for x in self.states:
            for c in self.alphabet:
                for y in self.transition(x, c):
                    if expr[x, y]:
                        expr[x, y] += '+' + str(c)
                    else:
                        expr[x, y] = str(c)

        for s in self.states:
            states.remove(s)
            for x in states:
                for y in states:
                    if expr[x, s] is not None and expr[s, y] is not None:
                        xsy = []
                        if expr[x, s]:
                            xsy += self._add_parentheses(expr[x, s])
                        if expr[s, s]:
                            xsy += self._add_parentheses(expr[s, s], True) + ['*']
                        if expr[s, y]:
                            xsy += self._add_parentheses(expr[s, y])
                        if expr[x, y] is not None:
                            xsy += ['+', expr[x, y] or '()']
                        expr[x, y] = ''.join(xsy)
        result = expr[initial, final]
        for l in self.alphabet:
            result = result.replace(f'{l}{l}*+()', f'{l}*')
        return result

    @staticmethod
    def _add_parentheses(expr, starring=False):
        if len(expr) == 1 or (not starring and '+' not in expr):
            return [expr]
        elif starring and expr.endswith('+()'):
            return ['(', expr[:-3], ')']
        else:
            return ['(', expr, ')']


class ProductState:
    def __init__(self, automaton_a_set, automaton_b_set):
        self.automaton_a_set = automaton_a_set
        self.automaton_b_set = automaton_b_set

    def __eq__(self, other):
        return self.automaton_a_set == other.automaton_a_set and \
               self.automaton_b_set == other.automaton_b_set

    def __hash__(self):
        return hash(self.automaton_a_element) ^ hash(self.automaton_b_element)

    @property
    def automaton_a_element(self) -> str:
        return join_ordered(self.automaton_a_set)

    @property
    def automaton_b_element(self) -> str:
        return join_ordered(self.automaton_b_set)

    @property
    def value(self):
        return f"A({self.automaton_a_element}),B({self.automaton_b_element})"


if __name__ == '__main__':
    # nfa = Nfa(
    #     'q0',
    #     {'q5'},
    #     {
    #         'q0': {
    #             '$': {'q1', 'q2'},
    #         },
    #         'q1': {
    #             '0': {'q3'}
    #         },
    #         'q2': {
    #             '1': {'q3'}
    #         },
    #         'q3': {
    #             '$': {'q0'},
    #             '0': {'q4'}
    #         },
    #         'q4': {
    #             '$': {'q5'}
    #         }
    #     }
    # )
    nfa = Nfa(
        'a',
        {'c', 'd', 'e'},
        {
            'a': {
                '0': {'b'},
                '1': {'c'}
            },
            'b': {
                '0': {'a'},
                '1': {'d'}
            },
            'c': {
                '0': {'e'},
                '1': {'f'}
            },
            'd': {
                '0': {'e'},
                '1': {'f'}
            },
            'e': {
                '0': {'e'},
                '1': {'f'}
            },
            'f': {
                '0': {'f'},
                '1': {'f'}
            }
        }
    )

    print(nfa)
