from copy import deepcopy
from enum import Enum
from typing import Optional, Tuple, List, Set, Iterable, Dict

from logic.utils import is_empty


class Direction(Enum):
    left = -1
    right = 1
    none = 0

    def __str__(self):
        if self is Direction.left:
            return 'L'
        elif self is Direction.right:
            return 'R'
        elif self is Direction.none:
            return 'N'


class Move:
    def __init__(self, *, origin: str, destiny: str, read: str, write: str, direction: Direction):
        self.origin = origin
        self.destiny = destiny
        self.read = read
        self.write = write
        self.direction = direction

    def __hash__(self) -> int:
        return hash(self.origin) ^ hash(self.destiny) ^ hash(self.read) ^ hash(self.write) ^ hash(self.direction)

    def __eq__(self, o: 'Move') -> bool:
        return o is self or (isinstance(o, Move) and self.origin == o.origin and self.destiny == o.destiny and
                             self.read == o.read and self.write == o.write and self.direction == o.direction)

    def __str__(self) -> str:
        return f"({self.origin}, {self.read}) -> ({self.destiny}, {self.write}, {self.direction})"


class Tape:
    def __init__(self, tape: Iterable[str], *, blank: str):
        self._blank = blank
        self._tape = {}
        for i, element in enumerate(tape):
            self._tape[i] = element

    def __hash__(self) -> int:
        return hash(self._blank)

    def __eq__(self, o: 'Tape') -> bool:
        return o is self or (isinstance(o, Tape) and self._tape == o._tape and self._blank == o._tape)

    def __getitem__(self, index: int) -> str:
        return self._tape.get(index, self._blank)

    def __setitem__(self, index: int, value: str):
        self._tape[index] = value

    def __str__(self) -> str:
        text = ''
        if not is_empty(self._tape.keys()):
            min_index = min(self._tape.keys())
            max_index = max(self._tape.keys())
            for i in range(min_index, max_index + 1):
                text += self[i]
        return text

    def replacing(self, index: int, value: str):
        copied = deepcopy(self)
        copied[index] = value
        return copied


class Step:
    def __init__(self, *, tape: Tape, state: str, head_position: int):
        self.tape = tape
        self.state = state
        self.head_position = head_position

    def __str__(self) -> str:
        return f"(tape: {self.tape}, state: {self.state}, head: {self.head_position})"

    def __hash__(self) -> int:
        return hash(self.tape) ^ hash(self.state) ^ hash(self.head_position)

    def __eq__(self, o: 'Step') -> bool:
        return o is self or (isinstance(o, Step) and self.tape == o.tape and self.state == o.state and
                             self.head_position == o.head_position)

    @property
    def value_under_head(self) -> Optional[str]:
        return self.tape[self.head_position]

    @property
    def marked_tape(self) -> str:
        tape_str = str(self.tape)
        marked_tape = ''
        for i, symbol in enumerate(tape_str):
            if i == self.head_position:
                marked_tape += f'({symbol})'
            else:
                marked_tape += symbol
        return marked_tape


class TuringMachine:
    def __init__(self):
        self.moves = set()
        self.acceptance_states = set()
        self.initial_state = None

    @staticmethod
    def from_inputs(
            initial_state: str,
            acceptance_states: Iterable[str],
            moves: Dict[str, Dict[str, Iterable[Tuple[str, str, str]]]]
    ) -> 'TuringMachine':

        new_machine = TuringMachine()
        new_machine.set_initial_state(initial_state)
        for state in acceptance_states:
            new_machine.add_acceptance_state(state)
        for (origin, transition) in moves.items():
            for (pop, results) in transition.items():
                for (destiny, push, direction) in results:
                    if direction in ('r', 'R'):
                        direction = Direction.right
                    elif direction in ('l', 'L'):
                        direction = Direction.left
                    else:
                        direction = Direction.none
                    new_move = Move(origin=origin, destiny=destiny, read=pop, write=push, direction=direction)
                    new_machine.add_move(new_move)
        return new_machine

    def add_move(self, move: Move):
        self.moves.add(move)

    def add_acceptance_state(self, state: str):
        self.acceptance_states.add(state)

    def set_initial_state(self, state: str):
        self.initial_state = state

    def find_moves(self, *, origin: str, read_value: str) -> Set[Move]:
        return set(move for move in self.moves if move.origin == origin and move.read == read_value)

    def process(self, tape: Tape) -> Optional[list]:
        if self.initial_state is None:
            raise BaseException('undefined initial state')

        initial_step = Step(tape=tape, state=self.initial_state, head_position=0)
        steps: Set[Step] = {initial_step}
        unprocessed_steps: List[Tuple[Step, list]] = [(initial_step, [])]

        while not is_empty(unprocessed_steps):
            step, snapshots = unprocessed_steps.pop()

            if step.state in self.acceptance_states:
                return snapshots + [step]

            for move in self.find_moves(origin=step.state, read_value=step.value_under_head):
                new_tape = step.tape.replacing(step.head_position, move.write)
                # noinspection PyTypeChecker
                new_head_position = step.head_position + move.direction.value

                next_step = Step(tape=new_tape, state=move.destiny, head_position=new_head_position)
                if next_step not in steps:
                    steps.add(next_step)
                    unprocessed_steps.append((next_step, snapshots + [step]))

        return None


if __name__ == '__main__':
    (q0, q1, q2, q3, q4) = ("q0", "q1", "q2", "q3", "q4")

    turing = TuringMachine()
    turing.set_initial_state(q0)

    turing.add_move(Move(origin=q0, destiny=q1, read="0", write="X", direction=Direction.right))
    turing.add_move(Move(origin=q0, destiny=q3, read="Y", write="Y", direction=Direction.right))
    turing.add_move(Move(origin=q1, destiny=q1, read="Y", write="Y", direction=Direction.right))
    turing.add_move(Move(origin=q1, destiny=q1, read="0", write="0", direction=Direction.right))
    turing.add_move(Move(origin=q1, destiny=q2, read="1", write="Y", direction=Direction.left))
    turing.add_move(Move(origin=q2, destiny=q2, read="Y", write="Y", direction=Direction.left))
    turing.add_move(Move(origin=q2, destiny=q2, read="0", write="0", direction=Direction.left))
    turing.add_move(Move(origin=q2, destiny=q0, read="X", write="X", direction=Direction.right))
    turing.add_move(Move(origin=q3, destiny=q3, read="Y", write="Y", direction=Direction.right))
    turing.add_move(Move(origin=q3, destiny=q4, read="B", write="B", direction=Direction.right))

    turing.add_acceptance_state(q4)

    result = turing.process(Tape('01', blank='B'))
    if result is not None:
        for r in result:
            print(r)
