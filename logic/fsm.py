import logic.nfa


class Transition:
    def __init__(self, origin, value, destiny):
        self.origin = origin
        self.value = value
        self.destiny = destiny

    def __str__(self):
        return f'{self.origin}, {self.value} => {self.destiny}'


def join_ordered(iterable: iter, separator: str = ',') -> str:
    return separator.join(str(i) for i in sorted(iterable))


epsilon = '$'


class Automaton:
    def __init__(self):
        self.states = set()
        self.alphabet = set()
        self.acceptance_states = set()
        self.initial_state = None
        self.transitions = set()

    @staticmethod
    def from_old_nfa(nfa):
        automaton = Automaton()
        for origin, transitions in nfa.transitions.items():
            for symbol, destinies in transitions.items():
                for destiny in destinies:
                    automaton.add_transition(origin, symbol, destiny)

        for acceptance_state in nfa.finals:
            automaton.add_acceptance_state(acceptance_state)

        automaton.set_initial_state(nfa.start)

        return automaton

    def to_old_nfa(self):
        transitions = {}
        for state in self.states:
            transitions[state] = {}
            for symbol in self.alphabet.union({epsilon}):
                transitions[state][symbol] = set()
                for t in self.find_transitions(origin=state, value=symbol):
                    transitions[state][symbol].add(t.destiny)

        return logic.nfa.Nfa(self.initial_state, self.acceptance_states, transitions)

    def clone(self):
        clone = Automaton()
        for transition in self.transitions:
            clone.add_transition(transition.origin, transition.value, transition.destiny)
        for state in self.acceptance_states:
            clone.add_acceptance_state(state)
        clone.set_initial_state(self.initial_state)

    def add_state(self, state):
        self.states.add(state)

    def add_symbol(self, symbol):
        if symbol != epsilon:
            self.alphabet.add(symbol)

    def add_acceptance_state(self, state):
        self.add_state(state)
        self.acceptance_states.add(state)

    def set_initial_state(self, state):
        self.add_state(state)
        self.initial_state = state

    def add_transition(self, origin, value, destiny):
        self.add_state(origin)
        self.add_state(destiny)
        if value != epsilon:
            self.add_symbol(value)

        self.transitions.add(Transition(origin, value, destiny))

    def find_transitions(self, *, origin=None, value=None, destiny=None) -> set:
        found_transitions = set()
        for transition in self.transitions:
            if (origin is None or transition.origin == origin) and (value is None or transition.value == value) and \
                    (destiny is None or transition.destiny == destiny):
                found_transitions.add(transition)
        return found_transitions

    def as_deterministic(self):
        new_automaton = Automaton()

        start = self.compute_epsilon_closure([self.initial_state])
        new_automaton.set_initial_state(join_ordered(start))
        if any(state in self.acceptance_states for state in start):
            new_automaton.add_acceptance_state(join_ordered(start))

        unprocessed_states = [start]
        processed_states = [start]
        while unprocessed_states:
            current_state = unprocessed_states.pop()
            for symbol in self.alphabet:
                destiny_state = self.move(current_state, symbol)
                if not destiny_state:
                    continue

                if destiny_state not in processed_states:
                    processed_states.append(destiny_state)
                    unprocessed_states.append(destiny_state)

                new_automaton.add_transition(join_ordered(current_state), symbol, join_ordered(destiny_state))

                if any(state in self.acceptance_states for state in destiny_state):
                    new_automaton.add_acceptance_state(join_ordered(destiny_state))

        return new_automaton

    def reverse(self):
        reversed_automaton = Automaton()

        if self.initial_state:
            reversed_automaton.add_acceptance_state(self.initial_state)

        if self.acceptance_states:
            # reversed_initial_state = next(iter(self.acceptance_states))
            reversed_initial_state = 'INIT'
            reversed_automaton.set_initial_state(reversed_initial_state)
            for acceptance_state in self.acceptance_states:
                if acceptance_state != reversed_initial_state:
                    reversed_automaton.add_transition(reversed_initial_state, epsilon, acceptance_state)

        for t in self.transitions:
            reversed_automaton.add_transition(t.destiny, t.value, t.origin)

        return reversed_automaton

    def complement(self):
        automaton_complement = Automaton()
        automaton_complement.set_initial_state(self.initial_state)
        automaton_complement.add_acceptance_state('$END$')

        for state in self.states.difference(self.acceptance_states):
            automaton_complement.add_acceptance_state(state)

        for transition in self.transitions:
            automaton_complement.add_transition(transition.origin, transition.value, transition.destiny)

        for state in self.states:
            for symbol in self.alphabet:
                if len(self.find_transitions(origin=state, value=symbol)) == 0:
                    automaton_complement.add_transition(state, symbol, '$END$')

        return automaton_complement

    def union(self, other):
        def is_valid(state_product: ProductState):
            return any(state in self.acceptance_states for state in state_product.automaton_a_set) or \
                   any(state in other.acceptance_states for state in state_product.automaton_b_set)
        return self.product(other, is_valid)

    def intersection(self, other):
        def is_valid(state_product):
            return any(state in self.acceptance_states for state in state_product.automaton_a_set) and \
                   any(state in other.acceptance_states for state in state_product.automaton_b_set)
        return self.product(other, is_valid)

    def difference(self, other):
        def is_valid(state_product):
            return any(state in self.acceptance_states for state in state_product.automaton_a_set) and \
                   not any(state in other.acceptance_states for state in state_product.automaton_b_set)
        return self.product(other, is_valid)

    def product(self, other, is_acceptance):
        start_state = ProductState(self.compute_epsilon_closure({self.initial_state}),
                                   other.compute_epsilon_closure({other.initial_state}))

        automata_product = Automaton()
        automata_product.set_initial_state(start_state.value)

        if is_acceptance(start_state):
            automata_product.add_acceptance_state(start_state.value)

        unprocessed_states = [start_state]
        while unprocessed_states:
            current_state = unprocessed_states.pop()
            for symbol in self.alphabet.union(other.alphabet):
                self_destinies = self.move(current_state.automaton_a_set, symbol, perform_closure=True)
                other_destinies = other.move(current_state.automaton_b_set, symbol, perform_closure=True)
                state_product = ProductState(self_destinies, other_destinies)

                if state_product.value not in automata_product.states:
                    automata_product.add_state(state_product.value)
                    unprocessed_states.append(state_product)

                    if is_acceptance(state_product):
                        automata_product.add_acceptance_state(state_product.value)

                automata_product.add_transition(current_state.value, symbol, state_product.value)

        return automata_product

    def minimize(self):
        minimized = self.reverse().as_deterministic().reverse().as_deterministic()
        return minimized

    def compute_epsilon_closure(self, from_states):
        reached_states = set(from_states)
        unprocessed_states = list(from_states)

        while unprocessed_states:
            current_state = unprocessed_states.pop()
            for state in self.make_simple_move(current_state, epsilon):
                if state not in reached_states:
                    reached_states.add(state)
                    unprocessed_states.append(state)

        return reached_states

    def move(self, origins, value, perform_closure=True):
        if perform_closure:
            return self.make_move_with_closure(origins, value)
        else:
            return self.make_multiple_simple_moves(origins, value)

    def make_simple_move(self, origin, value):
        return set(transition.destiny for transition in self.find_transitions(origin=origin, value=value))

    def make_multiple_simple_moves(self, origins, value):
        reached_states = set()
        for state in origins:
            destinies = self.make_simple_move(state, value)
            reached_states = reached_states.union(destinies)
        return reached_states

    def make_move_with_closure(self, origin, value):
        target_states = self.compute_epsilon_closure(origin)
        target_states = self.make_multiple_simple_moves(target_states, value)
        target_states = self.compute_epsilon_closure(target_states)
        return target_states

    def minimization(self):
        equivalence_table = {}
        for stateX in self.states:
            equivalence_table[stateX] = {}
            for stateY in self.states:
                equivalence_table[stateX][stateY] = None
                equivalence_table[stateY][stateX] = None

        for stateX in self.states:
            for stateY in self.states:
                if (stateX in self.acceptance_states and stateY not in self.acceptance_states) or \
                        (stateY in self.acceptance_states and stateX not in self.acceptance_states):
                    equivalence_table[stateX][stateY] = True
                    equivalence_table[stateY][stateX] = True


class ProductState:
    def __init__(self, automaton_a_set, automaton_b_set):
        self.automaton_a_set = automaton_a_set
        self.automaton_b_set = automaton_b_set

    def __eq__(self, other):
        return self.automaton_a_set == other.automaton_a_set and \
               self.automaton_b_set == other.automaton_b_set

    def __hash__(self):
        return hash(self.automaton_a_element) ^ hash(self.automaton_b_element)

    @property
    def automaton_a_element(self) -> str:
        return join_ordered(self.automaton_a_set)

    @property
    def automaton_b_element(self) -> str:
        return join_ordered(self.automaton_b_set)

    @property
    def value(self):
        return f"A({self.automaton_a_element}),B({self.automaton_b_element})"


if __name__ == '__main__':
    automaton = Automaton()
    automaton.set_initial_state('q0')
    automaton.add_acceptance_state('q3')
    automaton.add_acceptance_state('q4')

    automaton.add_transition('q0', '0', 'q1')
    automaton.add_transition('q0', '1', 'q1')
    automaton.add_transition('q1', '1', 'q2')
    automaton.add_transition('q1', '0', 'q3')
    automaton.add_transition('q2', '0', 'q4')
    automaton.add_transition('q2', '1', 'q1')
    automaton.add_transition('q3', '0', 'q4')
    automaton.add_transition('q3', '1', 'q1')
    automaton.add_transition('q4', '0', 'q3')
    automaton.add_transition('q4', '1', 'q2')

    minimized = automaton.minimize()

    print(minimized.initial_state)
    print(minimized.acceptance_states)

    for transition in minimized.transitions:
        print(transition)
